const redis = require("redis");

class RedisService {
  constructor(logger) {
    this.logger = logger;
    this.db = redis.createClient({
      host: process.env.REDIS_HOST || "127.0.0.1",
      port: 6379,
      retry_strategy: options => {
        if (options.attempt > 5) return null;
        return Math.min(options.attempt * 100, 3000);
      }
    });
    this.lifetime = 300;
    this.db.on("connect", () => {
      console.log("[REDIS::CONNECTED]");
    });

    this.db.on("error", err => {
      console.log("Error " + err);
    });

    this.db.on("end", function() {
      console.log("Redis connection closed!");
    });
  }

  async find(mask, max, min) {
    return await new Promise((resolve, reject) => {
      if (!this.db.connected) resolve(this.emptyReply());
      this.db.zrevrangebyscore([mask, max, min], (err, members) => {
        resolve(members);
      });
    });
  }

  async add(value) {
    if (!this.db.connected) return this.emptyReply();
    return this.db.zadd(value);
  }

  async remove(value) {
    if (!this.db.connected) return this.emptyReply();
    return this.db.zrem(value);
  }

  emptyReply() {
    return null;
  }
}

module.exports = new RedisService();
