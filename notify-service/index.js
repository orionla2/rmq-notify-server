const TransportService = require("./transport.service");
const RedisService = require("./redis.service");
const NotifyService = require("./notify.service");
class Server {
  static async build() {
    await TransportService.createConnection();
    await TransportService.connectIntoQueue("res-notify-service");
    return;
  }

  static async subscribe() {
    NotifyService.subscribe(async () => {
      const notifyArray = await RedisService.find(
        `timers`,
        Math.round(new Date().getTime() / 1000),
        0
      );
      if (notifyArray.length) Server.notify(notifyArray);
    });
  }

  static notify(notifyArray) {
    const notifyEntity = notifyArray.pop();
    console.log("[MESSAGE]", notifyEntity);
    RedisService.remove(["timers", notifyEntity]);
    if (notifyArray.length) Server.notify(notifyArray);
  }
}

Server.build()
  .then(() => {
    return Server.subscribe();
  })
  .catch(err => console.log("[Err]", err));
