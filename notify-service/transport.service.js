const rabbitmq = require("amqplib");
const RedisService = require("./redis.service");
class TransportService {
  constructor() {
    this.redis = RedisService;
  }

  connect() {
    return rabbitmq.connect("amqp://admin:123456789@rabbit-mq:5672");
  }

  async createConnection() {
    try {
      const connection = await this.connect();
      this.transport = connection;
      this.channel = await this.transport.createChannel();
      return connection;
    } catch (err) {
      return setTimeout(this.createConnection.bind(this, transport), 5000);
    }
  }

  async connectIntoQueue(queueName) {
    this.channel.assertQueue(queueName, {
      durable: false
    });
    this.channel.prefetch(1);
    this.channel.consume(queueName, async msg => {
      const body = JSON.parse(msg.content.toString());
      const ts = Math.round(new Date(body.time).getTime() / 1000);

      const status = await this.redis.add([
        `timers`,
        ts,
        JSON.stringify({ timestamp: ts, message: body.message })
      ]);
      const data = {
        message: "scheduled",
        status
      };
      this.channel.sendToQueue(
        msg.properties.replyTo,
        Buffer.from(JSON.stringify(data))
      );
      this.channel.ack(msg);
    });
  }
}

module.exports = new TransportService();
