# Notification Service

Notification server based on micro-services architecture. There are two micro-services: api-gateway and notify-service. Main technologies: Express, Redis, RabbitMQ, Docker, Nginx

# Instalation

1. `cd ./api-gateway`
2. `npm i`
3. `cd ../notify-service`
4. `npm i`
5. `cd ../`
6. `sudo chmod +x dev.sh build-images.sh`
7. `./build-images.sh`

# Docker interactive mode start

1. `./dev.sh`

# Webserver & API

POST /v1/echoAtTime HTTP/1.1
Host: localhost:80
Content-Type: application/json
Body: {
"message": "some",
"time": "2019-09-26T22:18:30.000Z"
}

# REDIS GUI

localhost:8081

# Conditions

The server has only 1 API:

echoAtTime - which receives two parameters, time and message, and writes that message to the server console at the given time.

The server to be able to withstand restarts it will use Redis to persist the messages and the time they should be sent at.
In case the server was down when a message should have been printed, it should print it out when going back online.

The focus of the exercise is:

- the efficient use of Redis and its data types

- messages should not be lost

- the same message should be printed only once

- message order should not be changed

- should be scalable

- seeing your code in action (SOLID would be a plus)

- use only redis.io
