#!/usr/bin/env bash
docker build -t rns-nginx-proxy:0.0.1 --rm -f Dockerfile-proxy .
docker build -t rns-api-gateway:0.0.1 --rm -f Dockerfile-api-gateway .
docker build -t rns-notify-service:0.0.1 --rm -f Dockerfile-notify-service .