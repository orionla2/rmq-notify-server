const EchoService = require("./echo.service");
class PingController {
  constructor() {
    this.service = EchoService;
  }

  async setEchoMessage(payload) {
    return await this.service.scheduleEchoEvent(payload);
  }
}

module.exports = new PingController();
