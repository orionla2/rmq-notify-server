const TransportService = require("../../services/transport.service");
const crypto = require("crypto");
class EchoService {
  constructor() {
    this.transport = TransportService;
  }

  async scheduleEchoEvent(payload) {
    try {
      return await new Promise((resolve, reject) => {
        this.transport.scheduleNotifyMessage({
          queueName: this.randomNumbers(),
          data: payload,
          callback: msg => {
            const responseStatus = JSON.parse(msg.content.toString());
            resolve(responseStatus);
          }
        });
      });
    } catch (err) {
      return err;
    }
  }

  randomNumbers() {
    const chars = "1234567890";
    const value = new Array(12);
    const len = chars.length;
    const data = crypto.randomBytes(12);
    for (let i = 0; i <= 12; i++) {
      value[i] = chars[data[i] % len];
    }
    return value.join("");
  }
}

module.exports = new EchoService();
