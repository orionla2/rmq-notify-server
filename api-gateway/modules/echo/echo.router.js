const Router = require("../../baseClasses/router");
const EchoController = require("./echo.controller");
class EchoRouter extends Router {
  constructor() {
    super();
    this.controller = EchoController;
    this.initRoutes();
  }

  initRoutes() {
    this.echoAtTime();
  }

  echoAtTime() {
    this.router.post("/", async (req, res) => {
      const insertStatus = await this.controller.setEchoMessage(req.body);
      if (insertStatus instanceof Error) {
        res.status(400).send({ error: insertStatus.message });
      } else {
        res.status(201).send(insertStatus);
      }
    });
  }
}

module.exports = new EchoRouter();
