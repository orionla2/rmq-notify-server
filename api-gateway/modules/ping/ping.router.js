const Router = require("../../baseClasses/router");
const PingController = require("./ping.controller");
class PingRouter extends Router {
  constructor() {
    super();
    this.controller = PingController;
    this.initRoutes();
  }

  initRoutes() {
    this.ping();
  }

  ping() {
    this.router.get("/", (req, res) => {
      const pingResponse = this.controller.getPingMessage();
      if (pingResponse instanceof Error) {
        res.status(400).send({ error: pingResponse.message });
      } else {
        res.status(200).send(pingResponse);
      }
    });
  }
}

module.exports = new PingRouter();
