class PingService {
  constructor() {}

  getPingMessage() {
    try {
      return { message: "pong" };
    } catch (err) {
      return err;
    }
  }
}

module.exports = new PingService();
