const PingService = require("./ping.service");
class PingController {
  constructor() {
    this.service = PingService;
  }

  getPingMessage() {
    return this.service.getPingMessage();
  }
}

module.exports = new PingController();
