const rabbitmq = require("amqplib");
class TransportService {
  constructor() {}

  connect() {
    return rabbitmq.connect("amqp://admin:123456789@rabbit-mq:5672");
  }

  async createConnection() {
    try {
      const connection = await this.connect();
      this.transport = connection;
      this.channel = await this.transport.createChannel();
      return connection;
    } catch (err) {
      return setTimeout(this.createConnection.bind(this, transport), 5000);
    }
  }

  async scheduleNotifyMessage(payload) {
    const { queueName, data, callback } = payload;
    const requesterQueue = `req-${queueName}`;
    const responderQueue = `res-notify-service`;
    await this.channel.assertQueue(requesterQueue, {
      exclusive: false,
      durable: false,
      autoDelete: true
    });
    this.channel.consume(requesterQueue, callback);
    this.send(responderQueue, requesterQueue, data);
  }

  send(targetQueue, replyTo, data) {
    this.channel.sendToQueue(targetQueue, Buffer.from(JSON.stringify(data)), {
      replyTo
    });
  }
}

module.exports = new TransportService();
