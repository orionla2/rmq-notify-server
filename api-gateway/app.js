const express = require("express");
const bodyParser = require("body-parser");
const EchoRouter = require("./modules/echo/echo.router");
const TransportService = require("./services/transport.service");
class Server {
  constructor(config) {
    this.app = config.app;
    this.transport = config.transport;
    this.router = new express.Router();
    this.EchoRouter = EchoRouter;
    this.prefix = "/v1";
    this.init();
    this.start();
  }

  static async build() {
    const app = express();
    const transport = await TransportService.createConnection();
    return { app, transport };
  }

  async init(app) {
    this.app.use(
      bodyParser.json({
        limit: "20mb"
      })
    );
    this.router.use("/echoAtTime", this.EchoRouter.router);
    this.app.use(this.prefix, this.router);
  }

  start(port = 3000) {
    this.app.listen(port, err => {
      if (err) console.log("[Server::err]", err);
      console.log("[Server::Started on PORT]", port);
    });
  }
}

Server.build().then(config => {
  const server = new Server(config);
});
